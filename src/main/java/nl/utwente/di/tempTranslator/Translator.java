package nl.utwente.di.tempTranslator;

public class Translator {

    public double getFahrenheit(String celsius) {
        return (Double.parseDouble(celsius) * 1.8 + 32);
    }
}
