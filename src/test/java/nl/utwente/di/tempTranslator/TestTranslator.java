package nl.utwente.di.tempTranslator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTranslator {

    @Test
    public void testTemp1() throws Exception {
        Translator translator = new Translator();
        double temp = translator.getFahrenheit("30");
        Assertions.assertEquals(86.0, temp,0.0,"Fahrenheit of 30ºC");
    }

}
